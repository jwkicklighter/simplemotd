package com.github.jwkicklighter;

/**
 * Simple Message of the Day Plugin
 * @author Jordan Kicklighter
 * @version 1
 */

import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class SimpleMOTD extends JavaPlugin {
	
	Logger log;
	public PluginManager pm;
	private SimpleMOTDPlayerListener playerListener = new SimpleMOTDPlayerListener(this);
	
	public void onEnable() {
		getConfig();
		log = getLogger();
		log.info(getConfig().getString("prefix") + " - " + getConfig().getString("motd"));
		
		pm = getServer().getPluginManager();
		pm.registerEvents(playerListener, this);
	}
	
	public void onDisable() {
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {
		Player player = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("motd")) {
			reloadConfig();
			sender.sendMessage(ChatColor.GOLD + getConfig().getString("prefix") + " - " + getConfig().getString("motd"));
			return true;
		}
		
		if(cmd.getName().equalsIgnoreCase("setmotd")) {
			
			if(args.length > 30) {
				sender.sendMessage(ChatColor.RED + "MOTD too long. Keep it to 30 words or less.");
				return false;
			}
			
			String newMotd = "";
			for(String arg : args)
				newMotd = String.format("%s %s", newMotd, arg);
			newMotd = newMotd.trim();
			
			getConfig().set("motd", newMotd);
			saveConfig();
			
			log.info(player.getDisplayName() + " changed the motd: \"" + newMotd + "\"");
			sender.sendMessage(ChatColor.GOLD + "MOTD Successfully Changed: \"" + newMotd + "\"");
			
			return true;
		}
		
		return false;
	}
	
}
