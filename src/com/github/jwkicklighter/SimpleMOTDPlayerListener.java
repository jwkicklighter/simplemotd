package com.github.jwkicklighter;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class SimpleMOTDPlayerListener implements Listener {
	
	private SimpleMOTD plugin;
	
	public SimpleMOTDPlayerListener(SimpleMOTD instance) {
		plugin = instance;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		
		player.sendMessage(ChatColor.GOLD + plugin.getConfig().getString("prefix") + " - " + plugin.getConfig().getString("motd"));
	}
	
}
